/**
 * JQuery Plugn: Browser Detection Extension
 * Description: Detects generic browser information and appends the appropriate browser name and version classes to the <html> element
 * Author: Eric Miller (email@pseudoninja.com) 
 * Version: 2012.02.01
 */
(function ($, navigator, document, window) {
    var constants = {
        classPrefix: 'browser-'
    }, browser = {
        ie: {
            classname: 'ie',
            getVersion: function () {
                return $.browser.version.substring(0, 1);
            },
            toString: function () {
                return browser.formatClassname(this.classname, this.getVersion());
            }
        },
        isIe: function () {
            return $.browser.msie;
        },
        chrome: {
            classname: 'chrome',
            getVersion: function (userAgent) {
                userAgent = userAgent || navigator.userAgent.toLowerCase();
                var version = userAgent.substring(userAgent.indexOf('chrome/') + 7);
                version = version.substring(0, version.indexOf('.'));
                // console.debug('chrome version', version); //@DEBUG
                return version;
            },
            toString: function () {
                browser.chrome.setNotSafari();
                return browser.formatClassname(this.classname, this.getVersion());
            },
            setNotSafari: function () {
                $.browser.safari = false;
                return;
            }
        },
        isChrome: function () {
            if ($.browser.chrome) {
                browser.chrome.setNotSafari();
            }
            return $.browser.chrome;
        },
        safari: {
            classname: 'safari',
            getVersion: function (userAgent) {
                userAgent = userAgent || navigator.userAgent.toLowerCase();
                var version = userAgent.substring(userAgent.indexOf('version/') + 8);
                return version.substring(0, 1);
            },
            toString: function () {
                return browser.formatClassname(this.classname, this.getVersion());
            }
        },
        isSafari: function () {
            return $.browser.safari;
        },
        mozilla: {
            classname: 'mozilla',
            toString: function () {
                return constants.classPrefix + this.classname;
            }
        },
        isMozilla: function () {
            return $.browser.mozilla;
        },
        firefox: {
            classname: 'firefox',
            getVersion: function (userAgent) {
                userAgent = userAgent || navigator.userAgent.toLowerCase();
                var version = userAgent.substring(userAgent.indexOf('firefox/') + 8);
                return version.substring(0, 1);
            },
            toString: function () {
                return browser.formatClassname(this.classname, this.getVersion());
            }
        },
        isFirefox: function (userAgent) {
            userAgent = userAgent || navigator.userAgent.toLowerCase();
            var isFirefox = false;
            if (browser.isMozilla() && userAgent.indexOf('firefox') != -1) {
                isFirefox = true;
            }
            return isFirefox;
        },
        opera: {
            classname: 'opera',
            toString: function () {
                return constants.classPrefix + this.classname;
            }
        },
        isOpera: function () {
            return $.browser.opera;
        },
        getUserAgent: function () {
            return navigator.userAgent.toLowerCase();
        },
        formatClassname: function (classname, version) {
            return constants.classPrefix + classname + ' ' + constants.classPrefix + classname + '-' + version;
        },
        get: function (prefix) {

            var brwsr = {
                classname: 'unknown',
                toString: function () {
                    return 'unknown';
                }
            };

            // IE
            if (browser.isIe()) {
                brwsr = browser.ie;
            } else if (browser.isMozilla()) { // Mozilla
                if (browser.isFirefox()) { // FireFox
                    brwsr = browser.firefox;
                } else {
                    brwsr = browser.mozilla;
                }
            } else if (browser.isOpera()) { // Opera
                brwsr = browser.opera;
            } else {
                if (browser.isChrome()) { // Chrome
                    brwsr = browser.chrome;
                } else if (browser.isSafari()) { // Safari
                    brwsr = browser.safari;
                }
            }
            return brwsr;
        }
    };
    
    if (($.browser.safari && /chrome/.test(navigator.userAgent.toLowerCase()))) { // chrome detection repair
        $.browser.chrome = true;
        $.browser.safari = false;
    } else {
        $.browser.safari = true;
        $.browser.chrome = false;
    }

    /**
     * Adds browser class to element
     */
    $.fn.addBrowserClass = function (prefix) {
        prefix = prefix || constants.classPrefix;
        var brwsr = browser.get(prefix);
        this.addClass(prefix + brwsr.classname + ' ' + prefix + brwsr.classname + '-' + brwsr.getVersion());
        return this;
    };

    $('html').addBrowserClass();

})(jQuery, navigator, document, this);